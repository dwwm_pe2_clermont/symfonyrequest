<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/cookies', name: 'cookie_')]
class CookieController extends AbstractController{
    #[Route('')]
    public function home(Request $request){
        return $this->render('cookies/index.html.twig',
            [
                "consent"=> $request->cookies->get('consent'),
                "theme" => $request->cookies->get('theme')
            ]
        );
    }

    #[Route('/consent', name: 'consent')]
    public function consent(){
        $response = new RedirectResponse("https://localhost:8000/cookies");
        $response->headers->setCookie(new Cookie("consent", "yes"));
        return $response;
    }

    #[Route('/theme/{color}',name: "themeswitch", requirements: ["color"=> "dark|white"])]
    public function changeTheme($color){
        $response = new RedirectResponse("https://localhost:8000/cookies");
        $response->headers->setCookie(new Cookie("theme", $color));
        return $response;
    }
}