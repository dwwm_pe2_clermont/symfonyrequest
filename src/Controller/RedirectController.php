<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/redirect', name: 'redirect_')]
class RedirectController extends AbstractController{

    #[Route('/{reseau}', requirements: ["reseau"=> "lk|x|fb"] )]
    public function redirectToSocialNetwork($reseau){
        $response = null;

        if($reseau == 'lk'){
            $response = new RedirectResponse("http://www.linkedin.com");
        }
        if ($reseau == 'x'){
            $response = new RedirectResponse("http://www.twitter.com");
        }
        if ($reseau == 'fb'){
            $response = new RedirectResponse("http://www.facebook.com");
        }

        return $response;
    }
}