<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/cv', name: "cv_")]
class CvController extends AbstractController{
    #[Route('/{format}', requirements: ['format'=> 'jpeg|pdf'])]
    public function downloadCv($format){
        $bResponse = null;

        if($format == 'jpeg'){
            $bResponse = new BinaryFileResponse("cv.jpg");
            $bResponse->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                "cv.jpg"
            );
        }

        if($format == 'pdf'){
            $bResponse = new BinaryFileResponse("cv.pdf");
            $bResponse->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                "cv.pdf"
            );
        }

        return $bResponse;
    }
}