<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/twig', name: 'twig_')]
class TwigController extends AbstractController{
    #[Route('', name: 'display')]
    public function displayTemplate(Request $request){
        $sessionVars = $request->getSession()->get("taches");

        return $this->render("twig/hello.html.twig", [
            "firstname"=> "Aurélien",
            "lastname"=> "Delorme",
            "sessionVars"=> $sessionVars
        ]);
    }
}