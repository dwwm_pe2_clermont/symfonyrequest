<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/session', name: 'session_')]
class SessionController extends AbstractController
{

    // Route qui permet d'aller ajouter une variable de session
    // permet également d'afficher toutes les variables de session
    // Elle prend en paramètre l'objet request puisque c'est lui qui contient la session
    #[Route('/add')]
    public function addSession(Request $request)
    {
        // Je réccupère ma session
        $session = $request->getSession();


        // Si ma méthode est de type POST
        // Je vais ajouter une valeur en session
        if ($request->getMethod() == 'POST') {
            // Je réccupére la clé saisie dans le formulaire
            $key = $request->request->get('cle');
            // Je réccupère a valeur saisie dans le form
            $value = $request->request->get('valeur');
            // J'ajoute ma données en session
            $session->set($key, $value);

            // Je redirige mon utilisateur une fois que j'ai ajouté ma session
            return new RedirectResponse("https://localhost:8000/session/add");
        }

        // Je retourne le contenu de ma page
        return $this->render("session/index.html.twig", [
            "sessionVars" => $request->getSession()->all()
        ]);
    }
}