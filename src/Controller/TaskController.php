<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/tasks', name: 'task_')]
class TaskController extends AbstractController{
    #[Route('/add', name: "add")]
    public function addTask(Request $request){
        // Si je suis dans une méthode de type post
        // Ca veut dire que mon formulaire a été soumis
        if($request->getMethod() == 'POST'){
           // J'initialise mon tableau de tache comme un nouveau tablau vide
           $tasks = [];
           $session = $request->getSession();

           // Si j'ai une clé taches dans ma session je réccupére les taches en session
           if($session->has("taches")){
               $tasks = $session->get('taches');
           }

           // J'ajoute la nouvelle tache
           $tasks[] = $request->request->get('task');
           // Je mets à jour ma session
           $session->set('taches', $tasks);

           // Je redirige l'utilisateur vers la liste de taches.
           return new RedirectResponse("https://localhost:8000/tasks/show");
        } else {
            return $this->render("tasks/add.html.twig");
        }

    }

    #[Route('/show', name: "show")]
    public function showTask(Request $request){
        // Je réccupére ma session dans ma requêtes
        $taches = $request->getSession()->get('taches');

        return $this->render('tasks/show.html.twig', [
            "taches"=> $taches
        ]);
    }

    #[Route('/delete/{index}')]
    public function deleteTask($index, Request $request){
        $session = $request->getSession();
        $taches = $session->get('taches');
        unset($taches[$index]);
        $session->set('taches', $taches);

        return new RedirectResponse('http://localhost:8000/tasks/show');
    }
}