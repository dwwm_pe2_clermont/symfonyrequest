<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/json', name: "json_")]
class JsonController extends AbstractController{
    #[Route('/{prenom}/{nom}')]
    public function jsonIdentity($prenom, $nom){
        $array = [
            "lastname"=> $nom,
            "firstname"=> $prenom
        ];

        return new JsonResponse($array);
    }
}